import ReactFlow, { Background, BackgroundVariant, ReactFlowProvider, useEdgesState, useNodesState, useReactFlow } from "react-flow-renderer"

const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {
      label:'Node 1'
    },
    position: {x: 250, y: 5}
  },
  {
    id: '2',
    data: {
      label:'Node 2'
    },
    position: {x: 100, y: 100}
  }
]

const initialEdges = [
  {id: 'e1-2', source: '1', target: '2'}
]

const defaultEdgeOptions = {
  animated: true,
}

const ControlledUncontrolled = () => {
  const [nodes, , onNodesChange] = useNodesState(initialNodes)
  const [edges, , onEdgesChange] = useEdgesState(initialEdges)
  const instance = useReactFlow()

  const logToObject = () => console.log(instance.toObject())

  const resetTransform = () => instance.setViewport({x: 0, y: 0, zoom: 1})

  const updateNodePositions = () => {
    instance.setNodes((nodes) =>
      nodes.map((node) => {
        node.position = {
          x: Math.random() * 400,
          y: Math.random() * 400,
        }
        return node
      })
    )
  }

  const updateEdgeColor = () => {
    instance.setEdges((edges) => 
      edges.map((edge) => {
        edge.style = {stroke: '#ff5050'}
        return edge
      })
    )
  }


  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      defaultEdgeOptions={defaultEdgeOptions}
      fitView
    >
      <Background variant={BackgroundVariant.Lines} />
      <div style={{position: 'absolute', zIndex: '4'}}>
        <button onClick={logToObject}>toObject</button>
        <button onClick={resetTransform}>reset transform</button>
        <button onClick={updateEdgeColor}>red edges</button>
        <button onClick={updateNodePositions}>change pos</button>
      </div>
    </ReactFlow>
  )
}

export default function App() {
  return (
    <ReactFlowProvider>
      <ControlledUncontrolled />
    </ReactFlowProvider>
  )
}