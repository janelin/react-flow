import ReactFlow, { ReactFlowProvider, useReactFlow } from "react-flow-renderer"

const onNodeDragStop = (_, node) => console.log('drag stop', node)
const onNodeClick = (_, node) => console.log('click', node)

const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {label: 'Node 1'},
    position: {x: 250, y: 100},
    className: 'light'
  },
  {
    id: '2',
    data: {label: 'Node 2'},
    position: {x:100, y:200},
    className: 'dark-node'
  },
  {
    id: '3',
    data: {label: 'Node 3'},
    position: {x:400, y:200},
    className: 'dark'
  }
]

const initialEdges = [
  {
    id: 'e1-2',
    source: '1',
    target: '2'
  },
  {
    id: 'e1-3',
    source: '1',
    target: '3'
  }
]

const Basic = () => {
  const instance = useReactFlow()

  const updatePos = () => {
    instance.setNodes((nodes) => 
      nodes.map((node) => {
        node.position = {
          x: Math.random() * 400,
          y: Math.random() * 400,
        }
        return node
      })
    )
  }

  const toggleClassnames = () => {
    instance.setNodes((nodes) => 
      nodes.map((node) => {
        node.className = node.className === 'light' ? 'dark' : 'light'
        return node
      })
    )
  }

  const resetTransform = () => {
    instance.setViewport({x: 0, y: 0, zoom: 1})
  }

  return (
    <ReactFlow
      defaultNodes={initialNodes}
      defaultEdges={initialEdges}
      onNodeDragStop={onNodeDragStop}
      onNodeClick={onNodeClick}
    >
      <div  style={{ position: 'absolute', zIndex: 4 }}>
        <button onClick={updatePos}>change pos</button>
        <button onClick={toggleClassnames}>toggle classnames</button>
        <button onClick={resetTransform}>reset transform</button>
      </div>
    </ReactFlow>
  )
}

export default function App() {
  return (
    <ReactFlowProvider>
      <Basic />
    </ReactFlowProvider>
  );
}
