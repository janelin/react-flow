import { useCallback } from "react"
import ReactFlow, {addEdge, useEdgesState, useNodesState,MiniMap, Controls} from "react-flow-renderer"

// init
const onInit = (reactFlowInstance) => console.log('pane ready:', reactFlowInstance)

// node events
const onNodeDragStart = (_, node, nodes) => console.log('drag start', node, nodes)
const onNodeDrag = (_, node, nodes) => console.log('drag', node, nodes)
const onNodeDragStop = (_, node, nodes) => console.log('drag stop', node, nodes)
const onNodeClick = (_, node) => console.log('node click', node)
const onNodeDoubleClick = (_, node) => console.log('node double click', node)
const onNodesDelete = (nodes) => console.log('nodes delete', nodes)

// edge events
const onEdgeContextMenu = (_, edge) => console.log('edge context menu', edge)
const onEdgeMouseEnter = (_, edge) => console.log('edge mouse enter', edge)
const onEdgeMouseMove = (_, edge) => console.log('edge mouse move', edge)
const onEdgeMouseLeave = (_, edge) => console.log('edge mouse leave', edge)
const onEdgeDoubleClick = (_, edge) => console.log('edge double click', edge)
const onEdgesDelete = (edges) => console.log('edges delete', edges)

// pane events
const onPaneClick = (event) => console.log('pane click', event)
const onPaneScroll = (event) => console.log('pane scroll', event)
const onPaneContextMenu = (event) => console.log('pane context menu', event)

// selection events
const onSelectionDragStart = (_, nodes) => console.log('selection drag start', nodes)
const onSelectionDrag = (_, nodes) => console.log('selection drag', nodes)
const onSelectionDragStop = (_, nodes) => console.log('selection drag stop', nodes)
const onSelectionChange = ({nodes, edges}) => console.log('selection change', nodes, edges)
const onSelectionContextMenu = (event, nodes) => {
  event.preventDefault();
  console.log('selection context menu', nodes)
}

// move events
const onMoveStart = (_, viewport) => console.log('zoom/move start', viewport)
const onMoveEnd = (_, viewport) => console.log('zoom/move end', viewport)


const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {
      label: (
        <>Welcome to <strong>React Flow!</strong></>
      )
    },
    position: {x: 250, y: 0}
  },
  {
    id: '2',
    data: {
      label: (
        <>This is a <strong>default node</strong> </>
      )
    },
    position: {x: 100, y:100},
    style: {borderRadius: '20px', borderColor: 'rgba(0,0,0,0.5)', color: '#156df2'}
  }
]

const initialEdges = [
  {
    id: 'e1-2',
    source: '1',
    target: '2',
    label: 'this is an edge label',
    type: 'smoothstep',
    style: {stroke: '#bdbdbd', width: '2px'},
    labelStyle: {fill: '#828282', fontSize: '12px'},
  }
]

// const connectionLineStyle = {stroke: '#ddd'}

// const nodeStrokeColor = (n) => {
//   if(n.style?.background) return n.style.background
//   if(n.type === 'input') return '#0041do'
//   if(n.type === 'output') return '#ff0072'
//   if(n.type === 'default') return '#1a192b'
//   return '#eee'
// }

// const nodeColor = (n) => {
//   if(n.style?.background) return n.style.background
//   return '#fff'
// }

const Overview = () => {
  const [nodes, , onNodesChange] = useNodesState(initialNodes)
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges)
  const onConnect = useCallback((params) => setEdges((eds) => addEdge(params, eds)),[setEdges])

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      onInit={onInit}
      onNodeDragStart={onNodeDragStart}
      onNodeDrag={onNodeDrag}
      onNodeDragStop={onNodeDragStop}
      onNodeClick={onNodeClick}
      onNodeDoubleClick={onNodeDoubleClick}
      onNodesDelete={onNodesDelete}
      onEdgeContextMenu={onEdgeContextMenu}
      onEdgeMouseEnter={onEdgeMouseEnter}
      onEdgeMouseMove={onEdgeMouseMove}
      onEdgeMouseLeave={onEdgeMouseLeave}
      onEdgeDoubleClick={onEdgeDoubleClick}
      onEdgesDelete={onEdgesDelete}
      onPaneClick={onPaneClick}
      onPaneScroll={onPaneScroll}
      onPaneContextMenu={onPaneContextMenu}
      onSelectionDragStart={onSelectionDragStart}
      onSelectionDrag={onSelectionDrag}
      onSelectionDragStop={onSelectionDragStop}
      onSelectionChange={onSelectionChange}
      onSelectionContextMenu={onSelectionContextMenu}
      onMoveStart={onMoveStart}
      onMoveEnd={onMoveEnd}
    >
      <MiniMap />
      <Controls />
    </ReactFlow>
  )
}

export default Overview