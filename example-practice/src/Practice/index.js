import { useEffect, useState } from "react"
import ReactFlow, { addEdge, Controls, MiniMap, useEdgesState, useNodesState } from 'react-flow-renderer';

import DefaultNode from "./nodes/DefaultNode";
import InputTextNode from './nodes/InputTextNode';
import EndingNode from './nodes/EndingNode'
import TenOutputsNode from './nodes/TenOutputsNode';

const FLOW_DIR = 'localhost';
const DB_PORT = 3001;

const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {label: 'input'},
    position: {x: 250, y: 5}
  },
  {
    id: '2',
    data: {label: 'Node 2'},
    position: {x: 100, y: 100}
  }
]

const initialEdges = [
  {id: 'e1-2', source: '1', target: '2'}
]

const nodeTypes = {
  inputText: InputTextNode,
  tenOutputs: TenOutputsNode,
  default: DefaultNode,
  ending: EndingNode,
  output: EndingNode,
};

const getId = () => `node_${Date.now()}`

const onDragStart = (event, nodeType) => {
  event.dataTransfer.setData('application/reactflow', nodeType)
  event.dataTransfer.effectAllowed = 'move'
}

const onDragOver = (event) => {
  event.preventDefault();
  event.dataTransfer.dropEffect = 'move'
}

const Practice = () => {
  const [reactFlowInstance, setReactFlowInstance] = useState()
  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes)
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges)
  
  const onInit = (rfInstance) => setReactFlowInstance(rfInstance)
  const onConnect = (params) => setEdges((eds) => addEdge(params, eds))
  const clearView = () => reactFlowInstance.setViewport({x: 100, y: 100, zoom: 1})

  useEffect(() => {
    const fetchFlow = async (setNodes, setEdges) => {
      // GET: /rest/workflows/[id]
      const res = await fetch(`http://${FLOW_DIR}:${DB_PORT}/posts/1`, {mode: 'cors'})
      const {nodes, edges} = await res.json()
      if (nodes) {
        // console.log('nodes', nodes);
        nodes.forEach((node) => {
            if (node.type === 'inputText') {
                node.data = { ...node.data, onInputChange };
            }
        });
        setNodes(nodes);
    }
    if (edges) {
        // console.log('edges', edges);
        setEdges(edges);
    }
    }
    fetchFlow(setNodes, setEdges)
  }, [])

  const storeFlow = async () => {
    await fetch(`http://${FLOW_DIR}:${DB_PORT}/posts/1`, {
        mode: 'cors',
        method: 'PUT',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify(reactFlowInstance?.toObject()),
    });
    console.log(reactFlowInstance?.toObject());
    alert('Flow saved!');
};

// custom node onChange event
const onInputChange = (event, id) => {
  setNodes((nds) =>
      nds.map((node) => {
          // only update node data with same id
          if (node.id === id) {
              node.data = { ...node.data, inputText: event.target.value };
          }

          return node;
      }),
  );
};

  const onDrop = (event) => {
    event.preventDefault()
    if(reactFlowInstance){
      const type = event.dataTransfer.getData('application/reactflow')
      const position = reactFlowInstance.project({
        x: event.clientX,
        y: event.clientY
      })
      const newNode = {
        id: getId(),
        type,
        position,
        data: {label: `${type} node`}
      }

      // pass onChange into custom node via data
      if (type === 'inputText') {
          newNode.data = {
              ...newNode.data,
              inputText: '',
              onInputChange,
          };
      }
      setNodes((nodes) => nodes.concat(newNode))
    }
  }

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onInit={onInit}
      onConnect={onConnect}
      onDragOver={onDragOver}
      onDrop={onDrop}
      nodeTypes={nodeTypes}
      fitView
    >
      <aside>
        <>
          {/* <div className='creator' onDragStart={(event) => onDragStart(event, 'input')} draggable>input</div> */}
          {/* <div className='creator' onDragStart={(event) => onDragStart(event, 'default')} draggable>default</div> */}
          <div className="creator" onDragStart={(event) => onDragStart(event, 'inputText')} draggable>
              input text
          </div>
          <div className="creator" onDragStart={(event) => onDragStart(event, 'tenOutputs')} draggable>
              ten outputs
          </div>
          <div className="creator" onDragStart={(event) => onDragStart(event, 'ending')} draggable>
              ending
          </div>
        </>
          <button onClick={clearView}>reset view</button>
          <br/>
          <button onClick={storeFlow}>save flow</button>
      </aside>
      <MiniMap />
      <Controls />
    </ReactFlow>
  )
}

export default Practice