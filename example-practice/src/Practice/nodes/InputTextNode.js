import { memo, useCallback } from 'react';
import { Handle, Position } from 'react-flow-renderer';

import NodeBase from './NodeBase';

const InputTextNode = ({ id, data }) => {
    const onChange = useCallback(
        (event) => {
            data?.onInputChange(event, id);
        },
        [data, id],
    );

    return (
        <NodeBase
            label={data.label}
            content={
                <>
                    <Handle type="target" position={Position.Left} />
                    <p>Say something:</p>
                    <input type="text" value={data?.inputText || ''} onChange={onChange} />
                    <Handle type="source" position={Position.Right} />
                </>
            }
        />
    );
};

export default memo(InputTextNode);
