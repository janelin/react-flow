import './index.css';

import { memo } from 'react';

const nodeStyle = {
    container: {
        width: 'auto',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#fff',
        boxShadow: '0 1px 4px 1px rgb(0 0 0 / 8%)',
        border: 'none',
        color: '#333',
        fontSize: '16px',
    },
    title: {
        borderRadius: '5px 5px 0 0',
        backgroundColor: '#bdbdbd',
        padding: '10px',
        textAlign: 'center',
    },
    body: {
        padding: '10px',
    },
};

const NodeBase = ({ label, content }) => {
    return (
        <div style={nodeStyle.container}>
            <div style={nodeStyle.title}>{label}</div>
            <div style={nodeStyle.body}>{content}</div>
        </div>
    );
};

export default memo(NodeBase);
