import { memo } from "react";
import { Handle, Position } from 'react-flow-renderer';

import NodeBase from "./NodeBase";

const EndingNode = ({data}) => {
  return (
    <NodeBase
      label={data.label}
      content={
        <>
          <Handle type="target" position={Position.Left} />
          <p></p>
          <Handle type="source" position={Position.Right} />
        </>
      }
    />
  )
}

export default memo(EndingNode)