import { memo } from 'react';
import { Handle, Position } from 'react-flow-renderer';

import NodeBase from './NodeBase';

const sourceHandleStyle1 = { top: 20, bottom: 'auto' };
const sourceHandleStyle2 = { top: 40, bottom: 'auto' };
const sourceHandleStyle3 = { top: 60, bottom: 'auto' };
const sourceHandleStyle4 = { top: 80, bottom: 'auto' };
const sourceHandleStyle5 = { top: 100, bottom: 'auto' };
const sourceHandleStyle6 = { top: 120, bottom: 'auto' };
const sourceHandleStyle7 = { top: 140, bottom: 'auto' };
const sourceHandleStyle8 = { top: 160, bottom: 'auto' };
const sourceHandleStyle9 = { top: 180, bottom: 'auto' };
const sourceHandleStyle10 = { top: 200, bottom: 'auto' };

const TenOutputsNode = ({ data }) => {
    let question = ''
    return (
        <NodeBase
            label={data.label}
            content={
                <div style={{ height: '200px' }}>
                    <Handle type="target" position={Position.Left} />
                    <p>{question}</p>
                    <Handle type="source" position={Position.Right} id={1} style={sourceHandleStyle1} />
                    <Handle type="source" position={Position.Right} id={2} style={sourceHandleStyle2} />
                    <Handle type="source" position={Position.Right} id={3} style={sourceHandleStyle3} />
                    <Handle type="source" position={Position.Right} id={4} style={sourceHandleStyle4} />
                    <Handle type="source" position={Position.Right} id={5} style={sourceHandleStyle5} />
                    <Handle type="source" position={Position.Right} id={6} style={sourceHandleStyle6} />
                    <Handle type="source" position={Position.Right} id={7} style={sourceHandleStyle7} />
                    <Handle type="source" position={Position.Right} id={8} style={sourceHandleStyle8} />
                    <Handle type="source" position={Position.Right} id={9} style={sourceHandleStyle9} />
                    <Handle type="source" position={Position.Right} id={10} style={sourceHandleStyle10} />
                </div>
            }
        />
    );
};

export default memo(TenOutputsNode);
