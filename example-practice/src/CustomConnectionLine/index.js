import ReactFlow, { addEdge, useEdgesState, useNodesState } from "react-flow-renderer"
import ConnectionLine from './ConnectionLine'

const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {
      label: 'Node 1'
    },
    position: {x: 250, y: 5}
  }
]

const initialEdges = []

const CustomConnectionLine = () => {
  const [nodes, , onNodesChange] = useNodesState(initialNodes)
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges)
  const onConnect = (params) => setEdges((eds) => addEdge(params, eds))
  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      connectionLineComponent={ConnectionLine}
    ></ReactFlow>
  )
}

export default CustomConnectionLine