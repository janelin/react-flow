import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes, useLocation, useNavigate } from 'react-router-dom';

import './index.css';
import Basic from './Basic';
import ControlledUncontrolled from './ControlledUncontrolled'
import CustomConnectionLine from './CustomConnectionLine'
import CustomNode from './CustomNode'
import DragNDrop from './DragNDrop'
import Practice from './Practice'
import Overview from './Overview'

const routes = [
  {
    path: '/', 
    component: Overview,
  },
  {
    path: '/basic',
    component: Basic,
  },
  {
    path: '/controlled-uncontrolled',
    component: ControlledUncontrolled,
  },
  {
    path: '/custom-connection-line',
    component: CustomConnectionLine,
  },
  {
    path: '/custom-node',
    component: CustomNode,
  },
  {
    path: '/drag-and-drop',
    component: DragNDrop,
  },
  {
    path: '/practice',
    component: Practice,
  }
]

const Header = () => {
  const navigate = useNavigate()
  const location = useLocation()
  const onChange = (event) => navigate(event.target.value)

  return (
    <header>
      <select defaultValue={location.pathname} onChange={onChange}>
        {routes.map((route) => (
          <option value={route.path} key={route.path}>
            {route.path === '/' ? 'overview' : route.path.substring(1, route.path.length)}
          </option>
        ))}
      </select>
    </header>
  )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Header />
    <Routes>
      {routes.map((route) => (
        <Route path={route.path} key={route.path} element={<route.component />} />
      ))}
    </Routes>
  </BrowserRouter>
);


