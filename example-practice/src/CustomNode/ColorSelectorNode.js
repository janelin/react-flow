import { memo } from "react"
import { Handle, Position } from "react-flow-renderer"

const targetHandleStyle = {background: '#555', width: '1px'}
const sourceHandleStyleA = {...targetHandleStyle, top: 10}
const sourceHandleStyleB = {...targetHandleStyle, bottom: 10, top: 'auto'}

const onConnect = (params) => console.log('handle onConnect', params)

const ColorSelectorNode = ({data, isConnectable}) => {
  return (
    <>
      <Handle type='target' position={Position.Left} style={targetHandleStyle} onConnect={onConnect} />
      <div>Custom Color Picker Node: <strong>{data.color}</strong></div>
      <input type='color' onChange={data.onChange} defaultValue={data.color} />
      <Handle type="source" position={Position.Right} style={sourceHandleStyleA} id='a' isConnectable={isConnectable} 
        onMouseDown={(e) => {console.log('You trigger mousedown event', e)}}
      />
      <Handle type="source" position={Position.Right} style={sourceHandleStyleB} id='b' isConnectable={isConnectable} />
    </>
  )
}

export default memo(ColorSelectorNode)