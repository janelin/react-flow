import { useEffect, useState } from "react"
import ReactFlow, { addEdge, Controls, MiniMap, Position, useEdgesState, useNodesState } from "react-flow-renderer"
import ColorSelectorNode from './ColorSelectorNode'

const onInit = (reactFlowInstance) => {
  console.log('flow loaded:', reactFlowInstance)
  reactFlowInstance.fitView()
}

const onNodeDragStop = (_, node) => console.log('drag stop', node)
const onNodeClick = (_, node) => console.log('click', node)

const initBgColor = '#1A192B'
const connectionLineStyle = {stroke: '#fff'}
const nodeTypes = {selectorNode: ColorSelectorNode}

const CustomNode = () => {
  const [nodes, setNodes, onNodesChange] = useNodesState([])
  const [edges, setEdges, onEdgesChange] = useEdgesState([])
  const [bgColor, setBgColor] = useState(initBgColor)

  useEffect(() => {
    const onChange = (event) => {
      setNodes((nodes) =>
        nodes.map((node) => {
          if(node.id !== '2') {
            return node
          }

          const color = event.target.value
          setBgColor(color)

          return {
            ...node,
            data: {
              ...node.data,
              color,
            }
          }
        })
      )
    }

    setNodes([
      {
        id: '1',
        type: 'input',
        data: {label: 'Input node'},
        position: {x: 0, y: 50},
        sourcePosition: Position.Right,
      },
      {
        id: '2',
        type: 'selectorNode',
        data: {onChange: onChange, color: initBgColor},
        style: {border: '1px solid #777', padding: 10},
        position: {x: 250, y: 50},
      },
      {
        id: '3',
        type: 'output',
        data: {label: 'Output A'},
        position: {x: 550, y: 25},
        targetPosition: Position.Left
      },
      {
        id: '4',
        type: 'output',
        data: {label: 'Output B'},
        position: {x: 550, y: 100},
        targetPosition: Position.Left
      }
    ])

    setEdges([
      {id: 'e1-2', source: '1', target: '2', style: {stroke: '#fff'}},
      {id: 'e2a-3', source: '2', sourceHandle: 'a', target: '3', style: {stroke: '#fff'}},
      {id: 'e2b-4', source: '2', sourceHandle: 'b', target: '4', style: {stroke: '#fff'}}
    ])
  }, [])

  const onConnect = (connection) => setEdges((edges) => addEdge({...connection}, edges))

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      onInit={onInit}
      onNodeDragStop={onNodeDragStop}
      onNodeClick={onNodeClick}
      connectionLineStyle={connectionLineStyle}
      nodeTypes={nodeTypes}
      style={{background: bgColor}}
    >
      <MiniMap 
        nodeStrokeColor={(n) => {
          if(n.type === 'input') return '#0041d0'
          if(n.type === 'selectorNode') return bgColor
          if(n.type === 'output') return '#ff0072'
          return '#eee'
        }}
        nodeColor={(n) => {
          if(n.type === 'selectorNode') return bgColor
          return '#fff'
        }}
      />
      <Controls/>
    </ReactFlow>
  )
}

export default CustomNode