import { useState } from "react"
import ReactFlow, { addEdge, useEdgesState, useNodesState } from "react-flow-renderer"
import './Sidebar'
import Sidebar from "./Sidebar"

const initialNodes = [
  {
    id: '1',
    type: 'input',
    data: {label: 'input node'},
    position: {x:250, y: 5}
  }
]

const getId = () => `node_${Date.now()}`

const onDragOver = (event) => {
  event.preventDefault();
  event.dataTransfer.dropEffect = 'move'
}

const DragNDrop = () => {
  const [reactFlowInstance, setReactFlowInstance] = useState()
  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes)
  const [edges, setEdges, onEdgesChange] = useEdgesState([])

  const onConnect = (params) => setEdges((eds) => addEdge(params, eds))
  const onInit = (rfInstance) => setReactFlowInstance(rfInstance)

  const onDrop = (event) => {
    event.preventDefault()

    if(reactFlowInstance) {
      const type = event.dataTransfer.getData('application/reactflow')
      const position = reactFlowInstance.project({
        x: event.clientX,
        y: event.clientY
      })
      const newNode = {
        id: getId(),
        type,
        position,
        data: {label: `${type} node`}
      }
      setNodes((nodes) => nodes.concat(newNode))
    }
  }

  return (
    <>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        onConnect={onConnect}
        onInit={onInit}
        onDragOver={onDragOver}
        onDrop={onDrop}
      >
      </ReactFlow>
      <Sidebar/>
    </>
  )
}

export default DragNDrop