const onDragStart = (event, nodeType) => {
  event.dataTransfer.setData('application/reactflow', nodeType)
  event.dataTransfer.effectAllowed = 'move'
}

const Sidebar = () => {
  return (
    <aside style={{position: 'absolute', zIndex: '4', top: 40, border: '1px solid #aaa'}}>
    <div onDragStart={(event)=> onDragStart(event, 'input')} draggable>input node</div>
    <div onDragStart={(event)=> onDragStart(event, 'default')} draggable>default node</div>
    <div onDragStart={(event)=> onDragStart(event, 'output')} draggable>output node</div>
  </aside>
  )
}

export default Sidebar